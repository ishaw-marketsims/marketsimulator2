package MS2;

/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author ishaw
 */
public class MarketFacilitator {

    private final Integer LENGTH_OF_NEGOTIATION = 6;    // number of steps in the negotiation
    private final Integer MAX_TRADE_SIZE = 10;          // most units that can be traded per round
    private final BuyerList buyerList;
    private final SellerList sellerList;
    private BidList bidList;

    public MarketFacilitator(BuyerList buyerList, SellerList sellerList) {
        this.buyerList = buyerList;
        this.sellerList = sellerList;
    }

    public String runNegotiation() { // returns a big string report of what happened
        String report = new String();
        bidList = new BidList(buyerList.getNumberOfBuyers());
        Integer numberOfActiveBuyers = 0;
        Boolean noBidsAccepted;
        int averageWinningBidPrice = 0;
        
        // get bids from buyers, populate the bidList
        // report += "Bids: ";
        for (Integer i = 0; i < bidList.getBidListSize(); i++) {
            if (buyerList.getBuyer(i).getDemandUnits() > 0) {//  if the buyer has any demand at all...
                bidList.addBid(buyerList.getBuyer(i).bidPrice(), buyerList.getBuyer(i));
                numberOfActiveBuyers++;
                // report += buyerList.getBuyer(i).getRef()+"("+buyerList.getBuyer(i).bidPrice() + "), ";
            }
        }
        report += "Bids ready: " + sellerList.getNumberOfSellers() + " sellers, " + bidList.getNumberOfBids() + " bids.\n";

        // System.out.println(report);
        // begin the negotiation        
        for (Integer i = 0; i < LENGTH_OF_NEGOTIATION; i++) {
            report += "Round " + (i + 1) + " of " + LENGTH_OF_NEGOTIATION + "\n";
            noBidsAccepted = true;
            Integer totalWinningBidPrices = 0;
            Integer numWinningBids = 0;
            
            // reset the remaining bids to not accepted
            report += "Buyers: ";
            for (Integer bid = 0; bid < bidList.getNumberOfBids(); bid++) {
                bidList.getBid(bid).setBidAccepted(false);
                // update the bid prices
                bidList.getBid(bid).getBuyer().updateBidPrice(averageWinningBidPrice);
                report += bidList.getBid(bid).getBuyer().getRef()+"("+bidList.getBid(bid).getBuyer().getMarketBidPrice() + "), ";
            }
            report += "\n";
            
            // update the sellers bids
            report += "Sellers: ";
            for (Integer seller = 0; seller<sellerList.getNumberOfSellers(); seller++) {
                if(averageWinningBidPrice != 0) {
                    sellerList.getSeller(seller).updateBidPrice(averageWinningBidPrice);
                    report += sellerList.getSeller(seller).getRef()+"("+sellerList.getSeller(seller).getMarketBidPrice()+"), ";
                } else {
                    report += sellerList.getSeller(seller).getRef()+"("+sellerList.getSeller(seller).getMarketBidPrice()+"), ";
                }
            }
            report += "\n";
            
            // each seller looks for a bid it can accept
            // if found, it accepts it, locking it for the rest of the round and fulfils as much demand as it can
            for (Integer j = 0; j < sellerList.getNumberOfSellers(); j++) {                             // for every seller
                if (sellerList.getSeller(j).getSupplyUnits() > 0) {                                     // if the seller has supply left to sell
                    for (Integer k = 0; k < bidList.getNumberOfBids(); k++) {                           // for every bid
                        if (!bidList.getBid(k).getBidAccepted()) {                                      // if the bid hasn't already been accepted
                            if (sellerList.getSeller(j).bidPrice() <= bidList.getBid(k).getBidPrice()   // and if the sales price is less or equal to the buyer bid price
                                    && sellerList.getSeller(j).getSupplyUnits() > 0) {                  // and another bidder hasn't bought out all the supply
                                bidList.getBid(k).setBidAccepted(true);                                 // accept the sale
                                noBidsAccepted = false;                                                 // something was accepted
                                Seller winningSeller = sellerList.getSeller(j);                         // (def to tidy up the code)
                                Buyer winningBuyer = bidList.getBid(k).getBuyer();                      // (def to tidy up the code)
                                Integer winningBidPrice = (winningSeller.bidPrice() + winningBuyer.bidPrice()) / 2; // set the unit price
                                totalWinningBidPrices += winningBidPrice;                               // keep track of the winning prices
                                numWinningBids++;
                                Integer salesVolume = doTrade(winningBuyer, winningSeller, winningBidPrice);
                                /*
                                if (winningSeller.bidAmount() <= winningBuyer.bidAmount()) {            // if supply is less than or equals demand, sell out and reduce demand
                                    salesVolume = winningSeller.bidAmount();
                                    winningSeller.addMoneyMade(winningBidPrice * salesVolume);
                                    winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
                                    winningBuyer.setDemandUnits(winningBuyer.bidAmount() - winningSeller.bidAmount());
                                    winningSeller.setSupplyUnits(0);
                                } else {                                                                // supply is more than demand, fully supply and reduce available supply left
                                    salesVolume = winningBuyer.bidAmount();
                                    winningSeller.addMoneyMade(winningBidPrice * salesVolume);
                                    winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
                                    winningSeller.setSupplyUnits(winningSeller.bidAmount() - winningBuyer.bidAmount());
                                    winningBuyer.setDemandUnits(0);
                                    bidList.deleteBid(winningBuyer);
                                }
*/
                                report += "Buyer " + winningBuyer.getRef() + " buys " + salesVolume + " from Seller " + winningSeller.getRef() + " at winning price: " + winningBidPrice + "\n";
                            }
                        }
                    }
                }
            }
            if (noBidsAccepted) {
                report += "No bids accepted this round.\n";
            } else {
                averageWinningBidPrice = (totalWinningBidPrices / numWinningBids);
                report += "Average winning bid price: " + averageWinningBidPrice + "\n\n";
            }

        }
        
        // restore seller supply
        for (Integer i = 0; i < sellerList.getNumberOfSellers(); i++)
            sellerList.getSeller(i).setSupplyUnits(sellerList.getSeller(i).getOriginalUnits());
            
        return report;
    }
    
    public Integer doTrade(Buyer winningBuyer, Seller winningSeller, Integer winningBidPrice) {
        // winningBuyer and winningSeller agreed to trade at winningBidPrice
        Integer salesVolume = Math.min(Math.min(winningBuyer.bidAmount(), winningSeller.bidAmount()), this.MAX_TRADE_SIZE);
        winningSeller.addMoneyMade(winningBidPrice * salesVolume);
        winningSeller.setSupplyUnits(winningSeller.bidAmount() - salesVolume);
        winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
        winningBuyer.setDemandUnits(winningBuyer.bidAmount() - salesVolume);
        if(winningBuyer.bidAmount() == 0)
            bidList.deleteBid(winningBuyer);
/*
        if (winningSeller.bidAmount() <= winningBuyer.bidAmount()) {            // if supply is less than or equals demand, sell out and reduce demand
            if(winningSeller.bidAmount() < this.MAX_TRADE_SIZE){
                salesVolume = winningSeller.bidAmount();
            } else {
                salesVolume = this.MAX_TRADE_SIZE;
            }
            winningSeller.addMoneyMade(winningBidPrice * salesVolume);
            winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
            winningBuyer.setDemandUnits(winningBuyer.bidAmount() - winningSeller.bidAmount());
            winningSeller.setSupplyUnits(0);
        } else {                                                                // supply is more than demand, fully supply and reduce available supply left
            if(winningBuyer.bidAmount() < this.MAX_TRADE_SIZE){
                salesVolume = winningBuyer.bidAmount();
            } else {
                salesVolume = this.MAX_TRADE_SIZE;
            }
            salesVolume = winningBuyer.bidAmount();
            winningSeller.addMoneyMade(winningBidPrice * salesVolume);
            winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
            winningSeller.setSupplyUnits(winningSeller.bidAmount() - winningBuyer.bidAmount());
            winningBuyer.setDemandUnits(0);
            bidList.deleteBid(winningBuyer);
        }
*/
    return salesVolume;
    }
    
}
